//
//  MLColorSliderPicker.swift
//  MLColorPicker
//
//  Created by Cao Phuoc Thanh on 12/07/2022.
//

import UIKit

public class MLColorSliderPicker: UIView {
    
    // MARK: callback
    public var colorOnChanged: ((UIColor) -> Void)? = nil
    
    // MARK: public properties
    @IBInspectable
    public var selectedColor: UIColor {
        get {
            guard self.colorView.bounds.width > 0 else { return self._selectedColor }
            return self.colorAtPoint(point: self.cursorView.center)
        }
        set {
            self._selectedColor = newValue
            guard self.colorView.bounds.width > 0 else { return }
            let x = self.getLocation(color: newValue)
            self.cursorView.center.x = x
            self.touchHandle(point: CGPoint(x: x, y: self.colorView.center.y))
        }
    }
    
    @IBInspectable
    public var throttle: TimeInterval = 0.0 {
        didSet {
            self.throttleDelay = Throttle(minimumDelay: self.throttle)
        }
    }
    
    // MARK: private properties'
    
    private var _selectedColor: UIColor = .red
    
    private var colors: [UIColor] = [
        UIColor(hex: 0xFF0000),
        UIColor(hex: 0xFFB700),
        UIColor(hex: 0x24FE00),
        UIColor(hex: 0x00FEEF),
        UIColor(hex: 0x0038FD),
        UIColor(hex: 0xFD00E4),
        UIColor(hex: 0xFD0000)
    ]
    
    private lazy var throttleDelay: Throttle = {
        return Throttle(minimumDelay: self.throttle)
    }()
    
    
    // MARK: UI
    private lazy var colorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private var colorslayer: CAGradientLayer = CAGradientLayer()
    
    private lazy var cursorView: UIView = {
        let view = UIView(frame: CGRect(x: 16, y: -4, width: 16, height: 16))
         view.layer.masksToBounds   = true
         view.layer.cornerRadius    = 8
         view.layer.borderColor     = UIColor.white.cgColor
         view.layer.borderWidth     = 1
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupComponents()
        self.addConstraints()
    }
    
    private func setupComponents() {
        self.backgroundColor = .clear
        self.addSubview(self.colorView)
        self.addSubview(cursorView)
        
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        self.colorView.layoutSubviews()
        self.colorslayer.frame = self.colorView.bounds
        let x = self.getLocation(color: self._selectedColor)
        self.cursorView.center = CGPoint(x: x, y: self.colorView.center.y)
    }
    
    private func addConstraints() {
        self.addConstraints([
            self.colorView.heightAnchor.constraint(equalToConstant: 2),
            self.colorView.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 0),
            self.colorView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            self.colorView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
        ])
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupComponents()
        self.addConstraints()
    }
    
    public override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.colorView.layer.insertLayer(
            layer: self.colorslayer,
            rect: self.colorView.bounds,
            colors: self.colors,
            opacity: 1,
            from: CGPoint(x: 0, y: 0.5),
            to: CGPoint(x: 1, y: 0.5),
            at: 0
        )
        
        let x = self.getLocation(color: self._selectedColor)
        self.cursorView.center = CGPoint(x: x, y: self.colorView.center.y)
        self.touchHandle(point: self.cursorView.center)
    }
}

// MARK: Touch Events
extension MLColorSliderPicker {
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first?.location(in: self.colorView) else { return }
        self.touchHandle(point: touch)
    }
    
    public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first?.location(in: self.colorView) else { return }
        self.touchHandle(point: touch)
    }
    
    public override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first?.location(in: self.colorView) else { return }
        self.touchHandle(point: touch, usethrottle: true)
    }
    
    private func touchHandle(point: CGPoint, usethrottle: Bool = false) {
        
        // validate point
        guard point.x >= 0 && point.x <= self.colorView.bounds.width else { return }
        
        let hue = point.x/self.colorView.bounds.width

        func handlePoint() {
            self._selectedColor = UIColor(hue: hue, saturation: 1, brightness: 1, alpha: 1)
            // set color cursor
            self.cursorView.backgroundColor = self._selectedColor
            
            self.colorOnChanged?(self._selectedColor)
        }
        
        if usethrottle && throttle != 0 {
            self.throttleDelay.throttle {
                handlePoint()
            }
        } else {
            handlePoint()
        }

        
        // move cursor
        self.cursorView.center.x = point.x
    
    }
    
    func colorAtPoint(point: CGPoint) -> UIColor {
        let hue = point.x/self.colorView.bounds.width
        let color = UIColor(hue: hue, saturation: 1, brightness: 1, alpha: 1)
        return color
    }
    
    func getLocation(color: UIColor) -> CGFloat {
        let x = self.colorView.bounds.width * color.hue.h
        return x
    }
}
