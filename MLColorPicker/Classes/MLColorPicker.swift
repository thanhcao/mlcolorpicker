//
//  MLColorPicker.swift
//  MLColorPicker
//
//  Created by Cao Phuoc Thanh on 12/07/2022.
//

import UIKit

public class MLColorPickerView: UIView {
    
    // MARK: callback
    public var colorOnChanged: ((UIColor) -> Void)? = nil
    
    // MARK: public properties
    @IBInspectable
    public var selectedColor: UIColor {
        get {
            return self.colorPanelPickerView.selectedColor
        }
        set {
            self.colorSliderPickerView.selectedColor = newValue
            self.colorPanelPickerView.selectedColor = newValue
        }
    }
    
    @IBInspectable
    public var throttle: TimeInterval {
        get {
            return self.colorSliderPickerView.throttle
        }
        set {
            self.colorSliderPickerView.throttle = newValue
            self.colorPanelPickerView.throttle = newValue
        }
    }
    
    // MARK: UI
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupConponents()
        self.setupContraints()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupConponents()
        self.setupContraints()
    }
    
    private lazy var colorSliderPickerView: MLColorSliderPicker = {
        let view = MLColorSliderPicker()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.colorOnChanged = { color in
            // change hue of colorPanelPickerView and keep saturation, brightness, alpha
            self.colorPanelPickerView.hue = color.hue.h
        }
        return view
    }()
    
    
    private lazy var colorPanelPickerView: MLColorPanelPicker = {
        let view = MLColorPanelPicker()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.colorOnChanged = {  [weak self] color in
            guard let self = self else { return }
            self.colorOnChanged?(color)
        }
        return view
    }()
    
    
    private func setupConponents() {
        self.addSubview(colorSliderPickerView)
        self.addSubview(colorPanelPickerView)
    }
    
    private func setupContraints() {
        
        self.addConstraints([
            colorSliderPickerView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
            colorSliderPickerView.heightAnchor.constraint(equalToConstant: 20),
            colorSliderPickerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            colorSliderPickerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
        ])
        
        self.addConstraints([
            colorPanelPickerView.bottomAnchor.constraint(equalTo: colorSliderPickerView.topAnchor, constant: -8),
            colorPanelPickerView.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            colorPanelPickerView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 8),
            colorPanelPickerView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8),
        ])
        
    }
    
    public override func draw(_ rect: CGRect) {
        super.draw(rect)
    }
    
}
