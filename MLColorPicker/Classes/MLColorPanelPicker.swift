//
//  MLColorPanelPicker.swift
//  MLColorPicker
//
//  Created by Cao Phuoc Thanh on 12/07/2022.
//

import UIKit

public class MLColorPanelPicker: UIView {
    
    // MARK: callback
    public var colorOnChanged: ((UIColor) -> Void)? = nil
    
    // MARK: public properties
    
    @IBInspectable
    public var selectedColor: UIColor {
        get {
            guard self.colorView.bounds.width > 0 else { return self._selectedColor }
            return self.colorAtPoint(point: self.cursorView.center)
        }
        set {
            guard self.colorView.bounds.width > 0 else {
                self._selectedColor = newValue
                self.saturationColors = [.white, UIColor(hue: newValue.hue.h, saturation: 1, brightness: 1, alpha: 1)]
                return
            }
            
            self.saturationColors = [.white, UIColor(hue: newValue.hue.h, saturation: 1, brightness: 1, alpha: 1)]
            self._selectedColor = newValue
            let point = self.getLocation(color: newValue)
            self.cursorView.center = point
        }
    }
    
    public var saturation: CGFloat {
        get {
            let oldhue = self._selectedColor.hue
            return oldhue.s
        }
        set {
            let oldhue = self._selectedColor.hue
            let hcolor = UIColor(hue: oldhue.h, saturation: newValue, brightness: oldhue.b, alpha: oldhue.a)
            self._selectedColor = hcolor
            let point = self.getLocation(color: hcolor)
            self.cursorView.center = point
        }
    }
    
    public var brightness: CGFloat {
        get {
            let oldhue = self._selectedColor.hue
            return oldhue.b
        }
        set {
            let oldhue = self._selectedColor.hue
            let hcolor = UIColor(hue: oldhue.h, saturation: oldhue.b, brightness: newValue, alpha: oldhue.a)
            self._selectedColor = hcolor
            let point = self.getLocation(color: hcolor)
            self.cursorView.center = point
        }
    }
    
    public var hue: CGFloat {
        get {
            let oldhue = self._selectedColor.hue
            return oldhue.h
        }
        set {
            let oldhue = self._selectedColor.hue
            let hcolor = UIColor(hue: newValue, saturation: oldhue.s, brightness: oldhue.b, alpha: oldhue.a)
            
            self.saturationColors = [.white, UIColor(hue: newValue, saturation: 1, brightness: 1, alpha: 1)]
            self._selectedColor = hcolor
            let point = self.getLocation(color: hcolor)
            self.cursorView.center = point
        }
    }
    
    @IBInspectable
    public var throttle: TimeInterval = 0.0 {
        didSet {
            self.throttleDelay = Throttle(minimumDelay: TimeInterval(self.throttle))
        }
    }
    
    //MARK: private properties
    
    private var _selectedColor: UIColor = UIColor(hue: 0, saturation: 0.5, brightness: 0.5, alpha: 1)
    
    private lazy var throttleDelay: Throttle = {
        return Throttle(minimumDelay: TimeInterval(self.throttle))
    }()
    
    private var saturationColors: [UIColor] = [.white, .red] {
        didSet {
            self.saturationColorsLayer.colors = self.saturationColors.map { $0.cgColor }
            self.pointHandle(point: self.cursorView.center, isCallReponse: true)
        }
    }
    
    // MARK: UI
    private var saturationColorsLayer: CAGradientLayer = CAGradientLayer()
    private var brightnessColorsLayer: CAGradientLayer = CAGradientLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupComponents()
    }
    
    private func setupComponents() {
        self.backgroundColor = .clear
        self.addSubview(self.colorView)
        self.addConstraints([
            self.colorView.topAnchor.constraint(equalTo: topAnchor, constant: 0),
            self.colorView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
            self.colorView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 0),
            self.colorView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0),
        ])
        
        self.addSubview(cursorView)
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupComponents()
    }
    
    private let colorView: UIView = {
       let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        self.saturationColorsLayer.frame = self.colorView.bounds
        self.brightnessColorsLayer.frame = self.colorView.bounds
        let point = self.getLocation(color: self._selectedColor)
        self.cursorView.center = point
    }
    
    private lazy var cursorView: UIView = {
        let view = UIView(frame: CGRect(x: 8, y: 8, width: 16, height: 16))
        view.layer.masksToBounds    = true
        view.layer.cornerRadius     = 8
        view.layer.borderColor      = UIColor.white.cgColor
        view.layer.borderWidth      = 1
        return view
    }()
    
    public override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        // add saturation layer
        self.colorView.layer.insertLayer(
            layer: self.saturationColorsLayer,
            rect: rect,
            colors: self.saturationColors,
            opacity: 1,
            from: CGPoint(x: 0, y: 0),
            to: CGPoint.init(x: 1, y: 0),
            at: 0
        )
        
        // add brightness layer
        self.colorView.layer.insertLayer(
            layer: self.brightnessColorsLayer,
            rect: rect,
            colors: [.black, .clear],
            opacity: 1,
            from: CGPoint(x: 0, y: 1),
            to: CGPoint(x: 0, y: 0),
            at: 1
        )
        
        let point = self.getLocation(color: self._selectedColor)
        self.cursorView.center = point
    }
}


// MARK: Touch Events
extension MLColorPanelPicker {
    
    public override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first?.location(in: self.colorView) else { return }
        self.pointHandle(point: touch, isCallReponse: true)
    }
    
    public override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first?.location(in: self.colorView) else { return }
        self.pointHandle(point: touch, isCallReponse: true)
    }
    
    public override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first?.location(in: self.colorView) else { return }
        self.pointHandle(point: touch, isCallReponse: true, usethrottle: true)
        
    }
    
    private func pointHandle(point: CGPoint, isCallReponse: Bool = false, usethrottle: Bool = false) {
        
        // validate point
        guard point.x >= 0 && point.x <= self.colorView.bounds.width else { return }
        guard point.y >= 0 && point.y <= self.colorView.bounds.height else { return }
        
        
        func handlePoint() {
            // get color from point
            self._selectedColor = self.colorAtPoint(point: point)
            
            // set color to curssor
            self.cursorView.backgroundColor = self._selectedColor
            
            if isCallReponse {
                // calback changed
                self.colorOnChanged?(self._selectedColor)
            }
        }
        
        
        if usethrottle && throttle != 0 {
            self.throttleDelay.throttle {
                handlePoint()
            }
        } else {
            handlePoint()
        }
        
        // move cursor
        self.cursorView.center = point
    }

    
    func colorAtPoint(point: CGPoint) -> UIColor {
        let hue = self.saturationColors[1].hue.h
        // 0 -> 1.0 : top to bottom colorlayer (colorVier)
        let brightness = 1 - point.y/self.colorView.bounds.height
        
        // 0 -> 1.0 : left to right colorlayer (colorVier)
        let saturation = point.x/self.colorView.bounds.width
        
        let color = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 1)
        return color
    }
    
    func getLocation(color: UIColor) -> CGPoint {
        let hue = color.hue
        let x = hue.s != 0 ? self.colorView.bounds.width*hue.s : 0
        let y = hue.b != 0 ? (self.colorView.bounds.height - self.colorView.bounds.height*hue.b) : 0
        return CGPoint(x: x, y: y)
    }
    
}
