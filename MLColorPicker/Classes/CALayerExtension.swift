//
//  CAlayerExtension.swift
//  MLColorPicker
//
//  Created by Cao Phuoc Thanh on 13/07/2022.
//

import UIKit

extension CALayer {
    
    func insertLayer(layer: CAGradientLayer, rect:CGRect, colors: [UIColor], opacity: Float, from: CGPoint, to: CGPoint, at: UInt32) {
        layer.colors        = colors.map { $0.cgColor}
        layer.startPoint    = from
        layer.endPoint      = to
        layer.frame         = rect
        layer.opacity       = opacity
        self.insertSublayer(layer, at: at)
    }
}
