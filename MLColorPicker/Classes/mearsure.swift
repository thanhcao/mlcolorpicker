//
//  mes.swift
//  MLColorPicker
//
//  Created by Cao Phuoc Thanh on 13/07/2022.
//

import Foundation

@discardableResult
public func mearsure<T>(name: String? = nil, _ block : (() -> T), file: String = #file, function : String = #function) -> T {
    #if DEBUG
    let from = Date().timeIntervalSince1970
    let result: T = block()
    let to = Date().timeIntervalSince1970
    let seconds = to - from
    print("[mearsure]","[\(Date())]","[\(file.components(separatedBy: "/").last ?? "")]","[\(Thread.current.name ?? "")] \(function)", name ?? "", "► \(seconds) seconds")
    return result
    #else
    let result: T = block()
    return result
    #endif
}
