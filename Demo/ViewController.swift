//
//  ViewController.swift
//  Demo
//
//  Created by Cao Phuoc Thanh on 12/07/2022.
//

import UIKit
import MLColorPicker

class ViewController: UIViewController {

    @IBOutlet weak var colorPickerViewStorboard: MLColorPickerView!
    @IBOutlet weak var trailinng: NSLayoutConstraint!
   
    
    private lazy var pickedColorView: UILabel = {
        let view = UILabel()
        view.layer.masksToBounds = true
        view.layer.borderColor = UIColor.white.cgColor
        view.layer.borderWidth = 1
        view.textAlignment = .center
        view.textColor = .orange
        view.font = UIFont.systemFont(ofSize: 12)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private lazy var colorView: MLColorPickerView = {
        let view = MLColorPickerView()
        view.throttle = 0.5
        view.selectedColor = UIColor(hue: 0.3, saturation: 0.3, brightness: 0.5, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
//    let colorView: MLColorPanelPicker = {
//        let view = MLColorPanelPicker()
//        view.selectedColor = UIColor(hue: UIColor.purple.hue.h, saturation: 0.3, brightness: 0.7, alpha: 1)
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
    
//    let colorView: MLColorSliderPicker = {
//        let view = MLColorSliderPicker()
//        view.selectedColor = .yellow
//        view.translatesAutoresizingMaskIntoConstraints = false
//        return view
//    }()
    
    override func loadView() {
        super.loadView()
        self.view.backgroundColor = .black
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.view.addSubview(self.colorView)
//
//        self.view.addConstraints([
//            self.colorView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -64),
//            self.colorView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 16),
//            self.colorView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -16),
//            self.colorView.heightAnchor.constraint(equalToConstant: 294)
//        ])
//
        self.view.addSubview(self.pickedColorView)
        self.view.addConstraints([
            self.pickedColorView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 64),
            self.pickedColorView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            self.pickedColorView.heightAnchor.constraint(equalToConstant: 50),
            self.pickedColorView.widthAnchor.constraint(equalToConstant: 200)
        ])
//
//
//        self.colorView.colorOnChanged = { color in
//            //print("colorOnChanged:", result)
//            self.pickedColorView.backgroundColor = color
//        }
        
        
        
        self.colorPickerViewStorboard.colorOnChanged = { color in
            //print("colorOnChanged:", result)
            self.pickedColorView.text = "\(color.hexString)"
            self.pickedColorView.backgroundColor = color
        }
    
    }


}

